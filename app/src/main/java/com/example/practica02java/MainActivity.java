package com.example.practica02java;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private EditText txtPeso;
    private EditText txtAltura;
    private TextView lblImc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /// NOTA: CORRREGIR EL RECURSO DE LAS R Y CHECAR EL NOMBRE DE CADA BOTON
        //Relación de los botones de java con los views xml
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLIMPIAR);
        btnCerrar = (Button) findViewById(R.id.btnCERRAR);
        //Relación de los txt de java con los views xml
        txtAltura = (EditText) findViewById(R.id.txtAlt);
        txtPeso = (EditText) findViewById(R.id.txtPes);
        lblImc = (TextView) findViewById(R.id.lblResultado);


        //Codificar el evento click del boton Calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Relacion con variables txt:
                String peso = txtPeso.getText().toString();
                String altura = txtAltura.getText().toString();
                //Declaracion de variables para la conversion
                float P = Float.parseFloat(peso);
                float A = Float.parseFloat(altura);

                //CALCULO DEL IMC:
                float IMC;

                if ((txtPeso.getText().toString().matches("")) || (txtAltura.getText().toString().matches(""))) {
                    //Falto capturar campos
                    Toast.makeText(MainActivity.this, "Rellena campos vacios!", Toast.LENGTH_SHORT).show();
                } else {
                    IMC = P / (A * A);
                    DecimalFormat formato1 = new DecimalFormat("#0.00");
                    System.out.println(formato1.format(IMC));
                    lblImc.setText(Float.toString(IMC)); //imprimir dato
                }

            }


    });

    //Codificar el evento click del boton limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View view){  //Aqui se realiza la programacion
        txtAltura.setText("");
        txtPeso.setText("");
    }
    });
    //Codificar el evento click del boton limpiar
        btnCerrar.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View view){  //Aqui se realiza la programacion
        finish();
    }
    });
}
}